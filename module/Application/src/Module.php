<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager=$e->getApplication()->getEventManager();
        $headers = $e->getRequest()->getHeaders();
        if ($headers->has('Origin')) {
            //convert to array because get method throw an exception
            $headersArray = $headers->toArray();
            $origin = $headersArray['Origin'];
            if ($origin === 'file://') {
                unset($headersArray['Origin']);
                $headers->clearHeaders();
                $headers->addHeaders($headersArray);
                //this is a valid uri
                $headers->addHeaderLine('Origin', 'file://mobile');
            }
        }
    }
}
